# L1J - Lineage I Java

***

### What is L1J?
L1J is Lineage I server emulation built in Java. This version is designed to support all languages as well as versions.

### What are the required prerequisites?
This version of L1J is built specifically for Ubuntu 12.04~14.04 (a FREE linux OS). Other prerequisites are: Apache2, MySQL, PHP, & Java 8.

### How to install?
too be continued